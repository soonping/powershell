<#
 .Synopsis
  Compares the two given files in the file paths using an MD5 hash, to determine if they are the same file.

 .Description
  Compares the two given files in the file paths using an MD5 hash, to determine if they are the same file.
  A return value of true or false will be given, depending on if the files are identical through their hashes.

 .Parameter filePathOne
  The path to the first file.

 .Parameter filePathTwo
  The path to the second file.

 .Example
   # Compare if two images are the same.
   Compare-Files -filePathOne C:\testfile.png -filePathTwo C:\testfile2.png
#>
function Get-EnvironmentVariables {
    (Get-ChildItem env:) | Format-List
}
Export-ModuleMember -Function Get-EnvironmentVariables