# EXAMPLE: .\OrderSplit.ps1 -userTotalCostsMapping @{"Ping" = 123.45; "Nathan" = 50.55; "Adan" = 23.49} -totalShipping 65.54 -exchangeRate 0.75

[CmdletBinding()]
param(
    # Mapping of a user's name and their total individual cost for the order (double). No shipping or other fees
    [Parameter(Mandatory)]
    [Hashtable]
    $userTotalCostsMapping,

    # Total shipping cost (Default 0.00)
    [Parameter(Mandatory=$false)]
    [decimal]
    $totalShipping = 0.00,

    # Exchange rate if you want the end result to be converted to a different exchange rate (Default 1.00 - no ER)
    [Parameter(Mandatory=$false)]
    [decimal]
    $exchangeRate = 1.00
)

# Variables Initialization
$totalCost = 0;
$finalUserCosts = @();

# Calculate Total Cost of order
foreach ($user in $userTotalCostsMapping.GetEnumerator()){
    $totalCost += $user.Value;
}

# Calculate each user's cost-weighted value for shipping
foreach ($user in $userTotalCostsMapping.GetEnumerator()){
    $shippingPercentage = $user.Value / $totalCost;
    $shippingCost = $totalShipping * $shippingPercentage;
    $finalCost = $user.Value + $shippingCost;
    $finalCostExchangeRate = $finalCost / $exchangeRate;

    $finalUserCost = [PSCustomObject]@{
        Name = $user.Key
        ShippingPercentage = [math]::Round($shippingPercentage, 2)
        TotalCost = [math]::Round($user.Value, 2)
        ShippingCost = [math]::Round($shippingCost, 2)
        FinalCost = [math]::Round($finalCost, 2)
        FinalCostExchangeRate = [math]::Round($finalCostExchangeRate, 2)
    };

    $finalUserCosts += $finalUserCost;
}

$overallCost = [math]::Round($totalShipping + ($totalCost / $exchangeRate), 2);

# Print out the full results of the calculations
Write-Output "==============================================
      Order split total cost calculations
=============================================="

Write-Output " - Total People: $($finalUserCosts.Count)"
Write-Output " - Total Cost: $totalCost"
Write-Output " - Shipping Cost: $totalShipping"
Write-Output " - Exchange Rate: $exchangeRate"
Write-Output " - Overall Cost (With Exchange Rate): $overallCost"

Write-Output "----------------------------------------------"

foreach ($finalUserCost in $finalUserCosts){
    Write-Output " "
    Write-Output "[$($finalUserCost.Name)]";
    Write-Output " - Total Product Cost: $($finalUserCost.TotalCost)";
    Write-Output " - Shipping Percentage: $($finalUserCost.ShippingPercentage)";
    Write-Output " - Shipping Cost: $($finalUserCost.ShippingCost)";
    Write-Output " - Final Cost: $($finalUserCost.FinalCost)";
    Write-Output " - Final Cost (With Exchange Rate): $($finalUserCost.FinalCostExchangeRate)";
    Write-Output " "
}

Write-Output "----------------------------------------------"