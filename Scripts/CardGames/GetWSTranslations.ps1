# EXAMPLE: .\GetWSTranslations.ps1 -deckCode IqMz_qaJB

[CmdletBinding()]
param(
    # The Encore Decks deck code (eg. the "IqMz_qaJB" part of the URL https://www.encoredecks.com/deck/IqMz_qaJB)
    [Parameter(Mandatory)]
    [string]
    $deckCode
)

$ErrorActionPreference = "Stop"

# Constants Initialization

# 0 = Deck code
$encoreDecksApiUrl = "https://www.encoredecks.com/api/deck/{0}";

# 0 = Set Name and code (eg. KS/W75-042)
$hotcTranslationUrl = "https://www.heartofthecards.com/code/cardlist.html?card=WS_{0}&short=1";

# Messages for display
$startingMessage = ">>> 🌐 Retrieving your WS HOTC translation screenshots. ";
$totalCardsToRetrieveMessage = ">>> Fetching HOTC screenshots, this may take a while. # of screenshots to retrieve: ";
$finishedRetrievingScreenShots = ">>> Finished fetching screenshots. Attempting to zip, and create PDF's for upload to Discord...";
$errorEncoreDecksMessage = ">>> ❌ **Error retrieving your deck from EncoreDecks or screenshots from HotC - Check your deck code, and ensure EncoreDecks and HoTC are accessible.**";

# Setup query and deck strings from user
$queryString = "https://www.encoredecks.com/api/deck/${deckCode}";
$deckString = "https://www.encoredecks.com/deck/${deckCode}";

#Import-Module "..\..\Resources\Tools\PowerWebShot\PowerWebShot.ps1"